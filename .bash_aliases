#!/bin/bash
# File containing all personal aliases

# cd's
alias uni='cd ~/Documents/Uni\ HD/'
alias ij='cd ~/Documents/Uni\ HD/Dr_J/Projects/ijcv_paper/ijcv-human-behaviour'
alias wp='cd ~/Documents/Uni\ HD/Dr_J/Projects/'
alias doas='cd ~/Documents/Uni\ HD/FP/CE\ DOAS/'
alias bsc='cd ~/Documents/Uni\ HD/Bachelorarbeit/git_repo/Bachelorarbeit_jhaux/'
alias ms='cd ~/Documents/Uni\ HD/Masterarbeit/'
alias v4='cd ~/Documents/Uni\ HD/Masterarbeit/code/glimpsenet/'
alias gn='cd ~/Documents/Uni\ HD/Masterarbeit/code/glimpsenet/'
alias t='cd /media/johannes/Data\ 1/Masterarbeit/Trainings/'
alias p='cd ~/Projekte/'
alias w='cd ~/Projekte/Wizard-Companion_2018/Wizard\ Companion/Assets/Scripts/'
# start special programs
alias nb='jupyter notebook'
alias gpu='watch -d -n 0,05 nvidia-smi'
alias vpn='/opt/cisco/anyconnect/bin/vpnui'
alias tb='tensorboard --logdir=.'
alias ipy='ipython3'
# Backup
alias b='cd ~/Documents/dotfiles; ./backup.sh; cd'
alias bs='cd ~/Documents/dotfiles; ./backup.sh; sudo shutdown now'
alias serve='python -m SimpleHTTPServer'
# Screen stuff
alias xbs='xrandr --output DP-0 --left-of DVI-I-1; xrandr --output DP-1 --left-of DVI-I-1;'
# ön/öff
alias hibernate='sudo systemctl hibernate'

alias unity3d="$HOME/software/Unity-2018.2.6f1/Editor/Unity"
alias kdenlive="cd $HOME/software/ && ./kdenlive-18.08.0-x86_64.AppImage && cd -"

alias vrtr='source ~/virtualenv/pt_track/bin/activate'
alias vrkp='source ~/virtualenv/kpscan/bin/activate'

alias sbc='source ~/.bashrc && echo sourced ~/.bashrc'

alias block_gpu="python -c 'import tensorflow as tf; tf.Session(); import time; time.sleep(3600)'"
function setup_remotes() {
    mkdir $HOME/remote
    mkdir $HOME/remote/cg2
    mkdir $HOME/remote/cv2
    sshfs quadxeon4:/ $HOME/remote/cg2/ -o follow_symlinks -o idmap=user
    sshfs compvisgpu03:/ $HOME/remote/cv2/ -o follow_symlinks -o idmap=user
}

alias remotefs='setup_remotes'

alias mount_jpc="mkdir $HOME/remote/jpc; sshfs johannes@129.206.117.74:/ $HOME/remote/jpc -o follow_symlinks -o idmap=user"

if command -v foo >/dev/null 2>&1; then
    alias tpl="xinput disable $(xinput --list | grep Touchpad | sed 's/.*id=\([^\t]*\).*/\1/')"
fi
alias wea='curl v2.wttr.in/Heidelberg'

alias ttt='bash ~/.i3/tex_setup.sh'

alias pap='cd /home/jhaux/Dr_J/Writing/CVPR/paper/moma/; git config --global credential.helper "cache --timeout=86400"; ttt'
