import os
import datetime


training_home = '/media/johannes/Data 1/Masterarbeit/Trainings'
backup_home = '/media/johannes/Backup RAID/Trainings'


def scan(home=training_home):
    backup_dirs = []
    for upper in os.listdir(home):
        upper_path = os.path.join(home, upper)
        sub_dirs = os.listdir(upper_path)
        sub_dirs = [os.path.join(upper_path, d) for d in sub_dirs]
        sub_dirs = [d for d in sub_dirs if os.path.isdir(d)]
        for training in sub_dirs:
            training = training.split('/')[-1]
            training_path = os.path.join(upper_path, training)
            if 'backup.token' in os.listdir(training_path):
                bu_path = os.path.join(backup_home, upper, training)
                bu_tuple = [training_path.replace(" ", "\\ "),
                            bu_path.replace(" ", "\\ ")]
                backup_dirs.append(bu_tuple)

    return backup_dirs


def write(backup_dirs):
    with open('backup_weights.sh', 'w+') as bu_f:
        now = datetime.datetime.now()
        bu_f.write('# Automatically created by scan_for_backups.py ')
        bu_f.write('on {}\n'.format(now.strftime("%Y-%m-%d %H:%M:%S")))

        for bu_dir in backup_dirs:
            bu_f.write('sudo mkdir -p ' + bu_dir[1] + '\n')
            bu_f.write('sudo rsync -aAX --info=progress2 --stats \\\n')
            bu_f.write('\t\t'+bu_dir[0]+'\\\n')
            bu_f.write('\t\t'+bu_dir[1]+'\n')
            bu_f.write('\n')


def make_bu_script(home=training_home):
    bu_dirs = scan(home)
    write(bu_dirs)


if __name__ == '__main__':
    make_bu_script()
