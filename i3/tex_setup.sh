if [ $# -eq 0 ]; then
    DOC=paper
else
    DOC=$1
fi

# function execute
#         if test (count $argv) -eq 0
#                 return 0
#         end
# 
#         if which $argv[1] > /dev/null
#                 eval $argv
#                 return $status
#         else
#                 return 1
#         end
# end

i3-msg 'split h'
sleep 0.2
$(okular $DOC.pdf &> /dev/null) &
sleep 0.2
i3-msg 'focus right, split v'
$(thunar . &> /dev/null) &
sleep 0.2
i3-msg 'focus left'
sleep 0.2
command="gnome-terminal -- latexmk -pvc"
echo $command
$command &> /dev/null &
sleep 0.2
i3-msg 'focus up, layout tabbed'
sleep 0.2
i3-msg 'focus left, focus left, move left, layout toggle split, layout toggle split'
sleep 0.2
vim ${PWD}/$DOC.tex
