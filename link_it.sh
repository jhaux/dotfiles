ROOT=$(realpath .)
echo "-- Root is $ROOT"

echo "-- linking ssh config"
mv $HOME/.ssh/config $HOME/.ssh/config.OLD
ln -s $ROOT/.ssh/config $HOME/.ssh/config

echo "-- linking vim config"
mv $HOME/.vimrc $HOME/.vimrc.OLD
ln -s $ROOT/.vimrc $HOME/.vimrc

COMPILE_YCM=false
if [ ! -d $HOME/.vim/bundle/Vundle.vim ]; then
    echo "-- cloning vundle"
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
    COMPILE_YCM=true
fi

echo "-- installing plugins"
vim +PluginInstall +qall

if [ "$COMPILE_YCM" = true ]; then
    echo "-- compiling YouCompleteMe"
    cd ~/.vim/bundle/YouCompleteMe
    python3 install.py --clang-completer
    cd $ROOT
fi

echo "-- installing tmux config"
mv $HOME/.tmux.conf $HOME/.tmux.conf.OLD
ln -s $ROOT/.tmux.conf $HOME/.tmux.conf

echo '-- linking bashrc and bash_aliases'
#;link
mv $HOME/.bashrc $HOME/.bashrc.OLD
ln -s $ROOT/.bashrc $HOME/.bashrc
mv $HOME/.bash_aliases $HOME/.bash_aliases.OLD
ln -s $ROOT/.bash_aliases $HOME/.bash_aliases
ln -s $ROOT/.profile $HOME/.profile

echo "-- installing powerline"
mkdir -p $HOME/.local/bin
pip install --user git+git://github.com/Lokaltog/powerline

wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
mkdir -p ~/.fonts/ && mv PowerlineSymbols.otf ~/.fonts/
fc-cache -vf
mkdir -p ~/.config/fontconfig/conf.d/ && mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/
