#!/bin/bash
# Based on the following webpage:
# https://wiki.archlinux.org/index.php/full_system_backup_with_rsync

echo
echo 'special files in Homefolder'
echo
sudo rsync -aAX --info=progress2 --stats \
        /home/johannes/Projekte/ \
        /media/johannes/Backup\ RAID/Ubuntu_Backup_Tower/home/johannes/Projekte/

sudo rsync -aAX --info=progress2 --stats \
        /home/johannes/Documents/ \
        /media/johannes/Backup\ RAID/Ubuntu_Backup_Tower/home/johannes/Documents/

sudo rsync -aAX --info=progress2 --stats \
        /home/johannes/Data\ 1/Masterarbeit/Datasets/validation_sets/ \
        /media/johannes/Backup\ RAID/Ubuntu_Backup_Tower/home/johannes/Data\ 1/Masterarbeit/Datasets/validation_sets/
echo 
echo '======================================================================='
echo 
echo 'weights within backup folders'
echo

python scan_for_backups.py
./backup_weights.sh

echo
echo '======================================================================='
echo
echo 'bootable backup, without homefolder'
echo
sudo rsync -aAX --info=progress2 --stats --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found","/home/*"} \
        / \
        /media/johannes/Backup\ RAID/Ubuntu_Backup_Tower/
