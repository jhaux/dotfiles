#bin/bash

# What could be done:
# make paths relative or ask for home path etc

# THIS SCRIPT IS UNTESTED
echo " ATTENTION: THIS SCRIPT IS UNTESTED"

sudo apt-get -y install manpages-pl

DOTFILES=$(dirname $(realpath $0))

echo "Dotfiles are located at:"
echo $DOTFILES

########
# htop #
########

echo
echo "installing htop"
sudo apt-get -y install htop


#######
# ssh #
#######

echo
echo "installing open-ssh"
sudo apt-get -y install openssh-server openssh-client
#link config
echo "-- installing ssh config"
mkdir $HOME/.ssh
ls -s $DOTFILES/.ssh/config $HOME/.ssh/config

#############
# Mono (C#) #
#############

echo
echo "installing mono (C#)"
sudo apt-get -y install mono-complete
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
sudo apt update


#########
# CMake #
#########

echo
echo "installing CMake"
# needed for YouCompleteMe plugin in vim
sudo apt-get -y install build-essential cmake


#######
# vim #
#######

echo
echo 'installing vim'
sudo apt-get -y install vim

# Install vundle
echo "-- installing vundle"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# link vimrc
echo "-- installing vimrc"
ln -s $DOTFILES/.vimrc $HOME/.vimrc

echo "-- installing vim plugins"
vim +PluginInstall +qall

# Still need to compile YCM
echo "-- compiling YouCompleteMe"
cd ~/.vim/bundle/YouCompleteMe
sudo ./install.py --clang-completer --omnisharp-completer


########
# tmux #
########

echo
echo 'installing tmux'
sudo apt-get -y install tmux

#link config
echo "-- installing tmux config"
ln -s $DOTFILES/.tmux.conf $HOME/.tmux.conf



###################
# Powerline setup #
###################

echo
echo 'installing powerline'
# from askubuntu.com/questions/283908/how-can-i-install-and-use-powerline-plugin
sudo pip install git+git://github.com/Lokaltog/powerline
sudo apt install -y powerline
#Powerline fonts
wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf
sudo mv PowerlineSymbols.otf /usr/share/fonts/
sudo fc-cache -vf
sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/


#######
# git #
#######

echo
echo 'linking gitconfig'
# link
ln -s $DOTFILES/.gitconfig $HOME/.gitconfig


########
# bash #
########

echo
echo 'linking bashrc and bash_aliases'
#;link
if [ -f $HOME/.bashrc ]; then
    rm $HOME/.bashrc
fi
ln -s $DOTFILES/.bashrc $HOME/.bashrc
ln -s $DOTFILES/.bash_aliases $HOME/.bash_aliases


##########
# thunar #
##########

echo
echo 'installing thunar'
sudo apt-get -y install thunar

# icons
sudo apt-add-repository universe
sudo apt-get update
sudo apt-get -y install gnome-icon-theme-full

# system theme
ln -s "$DOTFILES/gtk-2.0" "$HOME/.config/gtk-2.0"
ln -s "$DOTFILES/gtk-3.0" "$HOME/.config/gtk-3.0"
mv ~/.gtkrc-2.0 ~/.gtkrc-2.0.OLD
ln -s "$DOTFILES/.gtkrc-2.0" "$HOME/.gtkrc-2.0"

# Set thunar as default file manager
gvfs-mime --set inode/directory Thunar.desktop


#######
# pip #
#######

echo
echo "installing pip"
sudo apt-get -y install python-pip
sudo pip install --upgrade pip


###########
# ipython #
###########

echo
echo "installing ipython and notebook"
sudo pip install ipython

# notebook
sudo pip install jupyter


##############################
# ADOBE Source Code Pro font #
##############################

echo
echo "installing Adobe's Source Code Pro font"
mkdir /tmp/adodefont
cd /tmp/adodefont
wget https://github.com/adobe-fonts/source-code-pro/archive/2.010R-ro/1.030R-it.zip
unzip 1.030R-it.zip
mkdir -p ~/.fonts
cp source-code-pro-2.010R-ro-1.030R-it/OTF/*.otf ~/.fonts/
fc-cache -f -v


##########
# Cuda 8 #
##########

echo
echo "installing Cuda 8"
mkdir /tmp/cuda_install
cd /tmp/cuda_install
wget https://developer.nvidia.com/compute/cuda/8.0/prod/local_installers/cuda-repo-ubuntu1604-8-0-local_8.0.44-1_amd64-deb
sudo dpkg -i cuda-repo-ubuntu1604-8-0-local_8.0.44-1_amd64-deb
sudo apt-get update
sudo apt-get -y install cuda
cd 


#########
# cuDNN #
#########

# download cudnn from their page and copy the files to the right places
# see: http://askubuntu.com/questions/767269/how-can-i-install-cudnn-on-ubuntu-16-04


#######
# VLC #
#######

sudo apt-get -y install vlc
sudo apt-get -y install libxvidcore4 libfaac0


########
# CRON #
########

ln -s $DOTFILES/cron_wrapper $HOME/cron_wrapper

######
# i3 #
######

echo "You're almost done! Just install i3 following these steps:"
echo 'https://linoxide.com/gui/install-i3-window-manager-linux/'

sudo apt-get install i3
ln -s $DOTFILE/i3 $HOME/.i3

############
# nitrogen #
############

sudo apt-get -y install nitrogen

##########
# chrome #
##########

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo apt-get update
sudo apt-get -y install google-chrome-stable


###############
# pavucontrol #
###############

sudo apt-get -y install pavucontrol


#########
# scrot #
#########

sudo apt-get -y install scrot


########
# gimp #
########

sudo apt-get -y install gimp


###########
# inkscpe #
###########

sudo apt-get -y install inkscape


#############
# miniconda #
#############

wget -P $HOME/Downloads https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash $HOME/Downloads/Miniconda3-latest-Linux-x86_64.sh


###########
# xtrlock #
###########
# Locks you keyboard and mouse but not you screen!

sudo apt-get install xtrlock
