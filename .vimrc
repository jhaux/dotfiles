set shell=/bin/bash
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
"" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'

" Colorschemes and Highlighting
Plugin 'altercation/vim-colors-solarized'
Plugin 'morhetz/gruvbox'
Plugin 'kien/rainbow_parentheses.vim'
" Git integration
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'
" Better bindings
Plugin 'tpope/vim-unimpaired'
" Code completion with tab
" Plugin 'valloric/YouCompleteMe'
" Syntax checker
" Plugin 'vim-syntastic/syntastic'
" TODO Searcher
" Plugin 'vim-scripts/TaskList.vim'
" Indentation blocks line
Plugin 'Yggdroot/indentLine'
" latex-suite
" Plugin 'vim-latex/vim-latex'
" For Python code indentation and syntax checking
" Plugin 'klen/python-mode'
" IDE like vim
Plugin 'davidhalter/jedi-vim'
" Sidebar file browser
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
" Manipulate construct surroundings
Plugin 'tpope/vim-surround'
" Easy commenting functionality
Plugin 'tpope/vim-commentary'
" Powerline
Plugin 'powerline/powerline',  {'rtp': 'powerline/bindings/vim/'}

Plugin 'ervandew/supertab', {'rtp': 'plugin/'}

"" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
"" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
"" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
"" The sparkup vim script is in a subdirectory of this repo called vim.
"" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
"" Install L9 and avoid a Naming conflict if you've already installed a
"" different version somewhere else.
"Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Tabstop auf 4
set tabstop=4
set expandtab
set softtabstop=4

" show line numbers
set number

" show column at col 80
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" Colorscheme stuff
syntax enable
" let g:gruvbox_italic=1
set background=dark
colorscheme gruvbox
"colorscheme solarized

" Powerline stuff
" set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

" PASTETOGGLE!!!!
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" git-gutter
set updatetime=250

" " YouCompleteMe
" let g:ycm_autoclose_preview_window_after_completion=1

" " Syntastic
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*

" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0
" function! Py2()
"   let g:syntastic_python_python_exec = '/usr/bin/python2'
" endfunction
" 
" function! Py3()
"   let g:syntastic_python_python_exec = '/usr/bin/python3'
" endfunction
" 
" call Py3()   " default to Py3 because I try to use it when possible

" Spellchecking
" Tips :
"   get to next/prev misspelled: ]s / [s
"   show corrections: z= (1z= applies option 1 directly)
"   mark as correct/incorrect: zg / zw
" Toggle spell checking on and off with `,s`
let mapleader = ","
nmap <silent> <leader>s :set spell!<CR>
hi clear SpellBad
hi SpellBad cterm=underline,bold

" Rainbow Parentheses
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Indentation lines
set list lcs=tab:\|\ 

" Please show me all the latex code!
let g:tex_conceal = ""

" Open goto commands in tabs
let g:jedi#use_splits_not_buffers = 'right'


" """""""""""""""""" latex-suit / vim-latex stuff """""""""""""""""""""
" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
" filetype plugin on

" IMPORTANT: win32 users will need to have 'shellslash' set so that latex
" can be called correctly.
" set shellslash

" OPTIONAL: This enables automatic indentation as you type.
" filetype indent on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
" let g:tex_flavor='plaintex'
"
" """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Let's not use vimtex
" put \begin{} \end{} tags tags around the current word
map  <C-B>      YpkI\begin{<ESC>A}<ESC>jI\end{<ESC>A}<esc>kA
map! <C-B> <ESC>YpkI\begin{<ESC>A}<ESC>jI\end{<ESC>A}<esc>kA
map  <C-\>      o\item <ESC>A
map! <C-\> <ESC>o\item <ESC>A
let g:tex_flavor='latex'

" Nerdtree Stuff
map <leader>n :NERDTreeToggle<CR>

" docx util
autocmd BufReadPost *.docx :%!pandoc -f docx -t markdown
autocmd BufWritePost *.docx :!pandoc -f markdown -t docx % > tmp.docx
